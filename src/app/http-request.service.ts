import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(private http: HttpClient) { }

  generate() {
    return this.http.get('http://www.mocky.io/v2/5bd8d017310000c447475015?mocky-delay=10000ms');
  }
}
