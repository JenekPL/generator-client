import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {GeneratePageComponent} from './generate-page/generate-page.component';
import {RouterModule, Routes} from '@angular/router';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule, MatNativeDateModule, MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {HttpClientModule} from '@angular/common/http';
import {HttpRequestService} from './http-request.service';
import {GeneratePageDownloadCompleteComponent} from './generate-page/generate-page-download-complete.component';
import {GeneratePagePopupComponent} from './generate-page/generate-page-popup.component';

const appRoutes: Routes = [
  { path: '', component: GeneratePageComponent},
  { path: '**', component: NotFoundPageComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    GeneratePageComponent,
    GeneratePagePopupComponent,
    NotFoundPageComponent,
    GeneratePageDownloadCompleteComponent,
  ],
  entryComponents: [
    GeneratePageComponent,
    GeneratePagePopupComponent,
    GeneratePageDownloadCompleteComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatBottomSheetModule,
    AngularFontAwesomeModule,
    HttpClientModule,
  ],
  providers: [HttpRequestService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
