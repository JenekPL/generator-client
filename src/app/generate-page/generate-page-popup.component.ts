import {AfterViewInit, Component, OnInit} from '@angular/core';
import {HttpRequestService} from '../http-request.service';
import {MatBottomSheetRef, MatDialog} from '@angular/material';
import * as anime from 'animejs';
import {GeneratePageDownloadCompleteComponent} from './generate-page-download-complete.component';

@Component({
  selector: 'app-generate-page-popup',
  templateUrl: './generate-page-popup.component.html'
})
export class GeneratePagePopupComponent implements AfterViewInit, OnInit {
  constructor(private bottomSheetRef: MatBottomSheetRef<GeneratePagePopupComponent>,
              private httpRequestService: HttpRequestService,
              public dialog: MatDialog) {  }

  currentText = 0;
  texts = [
    'Stawianie ogrodzenia',
    'Układanie chodników',
    'Sprowadzanie zwierząt',
    'Zatrudnianie pracowników'
  ];

  ngOnInit(): void {
    this.httpRequestService.generate().subscribe(data => {
      console.log('We got this!', data);
      this.dialog.open(GeneratePageDownloadCompleteComponent);
      this.bottomSheetRef.dismiss();
    });
  }

  incCurrentText() {
    this.currentText++;
    if (this.currentText >= this.texts.length) { this.currentText = 0; }
    document.querySelectorAll('.animate').forEach(element => {
      element.textContent = this.texts[this.currentText];
    });
  }

  ngAfterViewInit(): void {
    const that = this;
    const animation = anime({
      targets: '.animate',
      left: [
        {value: 0, duration: 700},
        {value: 0, duration: 700},
        {value: '30rem', duration: 600}
      ],
      duration: 2000,
      loop: false,
      autoplay: true,
      delay: 2000,
      complete: function (anim) {
        that.incCurrentText();
        animation.restart();
      }
    });
  }
}
