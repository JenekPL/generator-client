import {Component} from '@angular/core';
import {MatBottomSheet} from '@angular/material';
import {GeneratePagePopupComponent} from './generate-page-popup.component';

@Component({
  selector: 'app-generate-page',
  templateUrl: './generate-page.component.html',
  styleUrls: ['./generate-page.component.css']
})
export class GeneratePageComponent {

  constructor(private bottomSheet: MatBottomSheet) { }

  dates = [
    {'name': 'T0', 'value': null},
    {'name': 'T1', 'value': null},
    {'name': 'T2', 'value': null},
  ];

  initialData = [
    {'name': 'Liczba Praconików', 'value': 100},
    {'name': 'Liczba Zwierząt', 'value': 10},
    {'name': 'Coś tam, coś tam', 'value': 4300},
    {'name': 'Coś tam, coś tam', 'value': 4300}
  ];

  symulationSettings = [
    {'name': 'Prawdopodobieństowo zwolnienia pracownika', 'value': 0.3},
    {'name': 'Prawdopodobieństow zatrudnienia pracownika', 'value': 0.4},
    {'name': 'Prawdopodobieństwo narodzenia nowego zwierzątka', 'value': 0.2},
    {'name': 'Prawdopodobieństowo czegoś na pewno', 'value': 0.6}
  ];

  openBottomSheet(): void {
    this.bottomSheet.open(GeneratePagePopupComponent);
  }

}

