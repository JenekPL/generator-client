import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';


@Component({
  selector: 'app-generate-page-download-complete',
  templateUrl: './generate-page-download-complete.component.html'
})
export class GeneratePageDownloadCompleteComponent {
  constructor(public dialogRef: MatDialogRef<GeneratePageDownloadCompleteComponent>) {  }

}
